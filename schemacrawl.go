package main

import (
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/gocrawl"
	"github.com/PuerkitoBio/goquery"
	"github.com/kennygrant/sanitize"
	"gopkg.in/mgo.v2"
	_ "gopkg.in/mgo.v2/bson"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"
)

type Product struct {
	Id          string `bson:"_id"`
	Context     string `json:"@context" bson:"@context"`
	Type        string `json:"@type" bson:"@type"`
	Name        string `json:"name" bson:"name"`
	Image       string `json:"image" bson:"image"`
	Description string `json:"description" bson:"description"`
	Brand       Brand  `json:"brand" bson:"brand"`
	Offers      Offers `json:"offers" bson:"offers"`
}

type Brand struct {
	Type string `json:"@type" bson:"@type"`
	Name string `json:"name" bson:"name"`
}

type Offers struct {
	Type     string  `json:"@type" bson:"@type"`
	Price    float64 `json:"price" bson:"price"`
	Currency string  `json:"priceCurrency" bson:"priceCurrency"`
}

type Ext struct {
	*gocrawl.DefaultExtender
}

func check(e error) {
	if e != nil {
		fmt.Println("Error encountered %v", e)
	}
}

func (e *Ext) Visit(ctx *gocrawl.URLContext, res *http.Response, doc *goquery.Document) (interface{}, bool) {
	doc.Find("script[type='application/ld+json']").Each(func(i int, s *goquery.Selection) {
		path := os.Getenv("GOPATH") + "/" + sanitize.Path(ctx.URL().String())
		err := os.MkdirAll(path, 0777)
		check(err)
		filePath := path + "/schema.json"
		f, err := os.Create(filePath)
		defer f.Close()
		check(err)
		err = ioutil.WriteFile(filePath, []byte(s.Text()), os.ModeExclusive)
		check(err)
		p := &Product{}
		err = json.Unmarshal([]byte(strings.Replace(s.Text(), "\n", "", -1)), &p)
		check(err)
		p.Id = ctx.URL().String()
		fmt.Println("Connecting")
		mongoSession, err := mgo.Dial(os.Getenv("MONGOLAB_URL"))
		defer mongoSession.Close()
		if err != nil {
			panic(err)
		}
		database := mongoSession.DB("crawler-test")
		collection := database.C("products")
		fmt.Println("Inserting")
		collection.Insert(p)
	})
	return nil, true
}

func (e *Ext) Filter(ctx *gocrawl.URLContext, isVisited bool) bool {
	if isVisited {
		return false
	}
	if ctx.URL().Host == "beretta.com" || ctx.URL().Host == "www.beretta.com" {
		return true
	}
	return false
}

func main() {
	ext := &Ext{&gocrawl.DefaultExtender{}}
	// Set custom options
	opts := gocrawl.NewOptions(ext)
	opts.CrawlDelay = 500 * time.Nanosecond
	opts.LogFlags = gocrawl.LogError

	c := gocrawl.NewCrawlerWithOptions(opts)
	c.Run("http://www.beretta.com/en-us/686-silver-pigeon-i-sporting/")
}
